# FredPHPMVC (v2)
Simple & Easy PHP Skeleton in MVC architecture.

FredPHPMVC (v2) is provided with four Controllers : Main, Datas, Errors & Infos.

**NOTE** : Used for my personnal usage to provide FredOS Datas in json format. FredOS JSON Datas are not available in this repository.

## FredUnit membership
**This project is a part of FredUnit, see installations recommandations, live-demo, thanks and credit here : https://fredericpetit.fr/unit.html.**